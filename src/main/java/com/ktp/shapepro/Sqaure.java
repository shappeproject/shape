/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ktp.shapepro;

/**
 *
 * @author acer
 */
public class Sqaure extends Shape{
    private double num;
    
    
    public Sqaure(double num){
        super("Sqaure");
        this.num = num;
    }

    public double getNum() {
        return num;
    }

    public void setNum(double num) {
        this.num = num;
    }

    @Override
    public String toString() {
        return "Sqaure{" + "num = " + num + '}';
    }
    
    @Override
    public double calArea(){
        return num*num;
    }
    
    
}
