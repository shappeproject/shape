/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ktp.shapepro;

/**
 *
 * @author acer
 */
public class Rectangle extends Shape{
    private double num1;
    private double num2;
    
    public Rectangle(double num1 ,double num2){
        super("Rectangle");
        this.num1 = num1;
        this.num2 = num2;
    }

    public double getNum1() {
        return num1;
    }

    public void setNum1(double num1) {
        this.num1 = num1;
    }

    public double getNum2() {
        return num2;
    }

    public void setNum2(double num2) {
        this.num2 = num2;
    }

    @Override
    public String toString() {
        return "Rectangle{" + "num1 = " + num1 + ", num2 = " + num2 + '}';
    }
    
    @Override
    public double calArea(){
        return num1*num2;
    }
    
}
